# -*- encoding: utf-8 -*-
{
    'name': 'Odoo remote agent gpio',
    'version': '12.0.1.2',
    'author': 'Odooist',
    'maintainer': 'Odooist',
    'support': 'odooist@gmail.com',
    'license': 'OPL-1',
    'price': 200.0,
    'category': 'Tools',
    'summary': 'Remote agent gpio stats collector',
    'description': "",
    'depends': ['remote_agent'],
    'data': [
        'views/assets.xml',
        'views/views.xml',
        'views/button.xml',
        'views/button_stats.xml',
        'views/ir_cron.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'images': [],
}
