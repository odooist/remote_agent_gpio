from datetime import datetime, timedelta
import gevent
import logging
import os
import random
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Date, Time
from sqlalchemy.orm import sessionmaker
from remote_agent import GeventAgent, public
if os.getenv('FAKE_BUTTONS') != '1':
    import RPi.GPIO as GPIO

logger = logging.getLogger(__name__)
DEBUG = bool(int(os.getenv('DEBUG', '0')))
SCRAPE_INTERVAL = int(os.getenv('SCRAPE_INTERVAL', '15'))  # 1 Second
SQL_ECHO = bool(int(os.getenv('SQL_ECHO', '0')))
PERIOD_1_START = os.getenv('PERIOD_1_START', '07:00')
PERIOD_2_START = os.getenv('PERIOD_2_START', '15:00')
PERIOD_3_START = os.getenv('PERIOD_3_START', '23:00')
UPDATE_STATS_INTERVAL_MINUTES = float(
    os.getenv('UPDATE_STATS_INTERVAL_MINUTES', '15'))

# Activate graylog
if os.getenv('GRAYLOG_ENABLED') == '1':
    logger.info('Activating Graylog')
    import graypy
    kwargs = {
        'localname': os.getenv('GRAYLOG_SOURCE', 'remote_agent_gpio'),
        'level_names': True
    }
    graypy_handler = graypy.GELFUDPHandler(
        os.getenv('GRAYLOG_HOST'),
        int(os.getenv('GRAYLOG_PORT')), **kwargs)
    logger.addHandler(graypy_handler)


Base = declarative_base()


# SQLite table for storing button states
class ButtonState(Base):
    __tablename__ = 'button_state'

    id = Column(Integer, primary_key=True)
    button_number = Column(Integer, nullable=False)
    state = Column(Integer, nullable=False)
    day = Column(Date, nullable=False)
    time = Column(Time, nullable=False)

    def __repr__(self):
        return '<State(button=%s>, day=%s, time=%s, state=%s' % (
            self.button_number, self.day, self.time, self.state)


class ButtonsAgent(GeventAgent):
    buttons = []  # List of button numbers

    def init_db(self):
        db_path = os.getenv('DB_PATH', os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'app.db'))
        engine_info = 'sqlite:///{}'.format(db_path)
        logger.info('Init db %s', engine_info)
        engine = sqlalchemy.create_engine(engine_info, echo=SQL_ECHO)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.session = Session()

    def get_state(self, button):
        if os.getenv('FAKE_BUTTONS') == '1':
            # return 0
            return int(random.choice('01'))
        try:
            state = GPIO.input(button)
            logger.debug('Button %s state is %s', button, state)
            return 1 - state  # GPIO returns 1 when off.
        except Exception:
            logger.exception('Button state read error')
            return 1

    def scrape(self):
        while True:
            gevent.sleep(SCRAPE_INTERVAL)
            logger.info('Scraping buttons: %s', self.buttons)
            now = datetime.now()
            for button in self.buttons:
                bs = ButtonState(
                    button_number=button,
                    state=self.get_state(button),
                    day=now.date(),
                    time=now.time())
                logger.debug('Scrape result: %s', bs)
                self.session.add(bs)
                self.session.commit()

    def get_time_period(self, t):
        t1 = datetime.strptime(PERIOD_1_START, '%H:%M').time()
        t2 = datetime.strptime(PERIOD_2_START, '%H:%M').time()
        t3 = datetime.strptime(PERIOD_3_START, '%H:%M').time()
        if t < t1:
            # First part of the 3-rd period
            logger.debug('Time period: 3')
            return 3.1
        elif t >= t1 and t < t2:
            # Period 1
            logger.debug('Time period: 1')
            return 1
        elif t >= t2 and t < t3:
            # Period 2
            logger.debug('Time period: 2')
            return 2
        else:
            # Period 3 part 2
            logger.debug('Time period: 3')
            return 3.2

    def update_stats(self):
        while True:
            logger.info('Update stats periodic run')
            logger.debug('Waiting for Odoo connection...')
            self.odoo_connected.wait()
            logger.debug('Got Odoo connection.')
            try:
                self.process_data()
            except Exception:
                logger.exception('Update stats error:')
            gevent.sleep(UPDATE_STATS_INTERVAL_MINUTES * 60)

    def process_data(self):
        processed_ids = []
        stats = {}
        for rec in self.session.query(
                ButtonState).all():
            period = self.get_time_period(rec.time)
            if period in [1, 2, 3.2]:
                day = rec.day
            elif period == 3.1:
                # Goes to previous day
                day = rec.day - timedelta(days=1)
            day_stats = stats.setdefault(str(day), {})
            day_button_stats = day_stats.setdefault(rec.button_number, {})
            key = 'period_{}_{}'.format(int(period), rec.state)
            if key in day_button_stats:
                day_button_stats[key] += 1
            else:
                day_button_stats[key] = 1
            processed_ids.append(rec.id)
        # Now update stats in Odoo and cleanup internal database
        self.odoo.env['remote_agent_gpio.button_stats'].update_stats(
            {'agent': self.agent_uid, 'data': stats})
        # Remove data
        processed_ids, self.session.query(ButtonState).filter(
            ButtonState.id.in_(
                processed_ids)).delete(synchronize_session='fetch')
        self.session.commit()

    @public
    def init_buttons(self):
        self.odoo_connected.wait()
        buttons_ids = self.odoo.env['remote_agent_gpio.button'].search(
            [('agent_uid', '=', self.agent_uid)])
        buttons = self.odoo.env['remote_agent_gpio.button'].browse(buttons_ids)
        self.buttons = [k.gpio_number for k in buttons]
        logger.info('Buttons init: %s', self.buttons)
        if os.getenv('FAKE_BUTTONS') != '1':
            GPIO.setmode(GPIO.BCM)
            for b in self.buttons:
                GPIO.setup(b, GPIO.IN)


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - '
               '%(name)s.%(funcName)s:%(lineno)d - %(message)s')
    logger.setLevel(level=logging.DEBUG if DEBUG else logging.INFO)
    ba = ButtonsAgent()
    ba.init_db()
    gevent.spawn(ba.init_buttons)
    gevent.spawn(ba.scrape)
    gevent.spawn(ba.update_stats)
    ba.start()
