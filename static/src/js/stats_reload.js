odoo.define("remote_agent_gpio.button_stats", function (require) {
    "use strict";
  
    var WebClient = require('web.WebClient');
    var ajax = require('web.ajax');
    var utils = require('mail.utils');
    var session = require('web.session');    
    var channel = 'remote_agent_gpio_button_stats';

    WebClient.include({
        start: function(){
            this._super()
            this.call('bus_service', 'addChannel', channel);
            this.call('bus_service', 'onNotification', this, this.on_gpio_button_stats_notification)
            console.log('Listening on remote_agent_gpio_button_stats.')
        },

        on_gpio_button_stats_notification: function (notification) {
          for (var i = 0; i < notification.length; i++) {
             var ch = notification[i][0]
             var msg = notification[i][1]
             if (ch == channel) {
                 try {
                  this.handle_gpio_button_stats_message(msg)
                }
                catch(err) {console.log(err)}
             }
           }
        },

        handle_gpio_button_stats_message: function(msg) {
          if (typeof msg == 'string')
            var message = JSON.parse(msg)
          else
            var message = msg
          var action = this.action_manager && this.action_manager.getCurrentAction()
          if (!action) {
              //console.log('Action not loaded')
              return
          }
          var controller = this.action_manager.getCurrentController()
          if (!controller) {
              //console.log('Controller not loaded')
              return
          }          
          if (controller.widget.modelName != "remote_agent_gpio.button_stats") {
            // console.log('Not button stats view')
            return
          }
          if (message['reload'] == true) {
            // console.log('Reload')
            controller.widget.reload()
          }
        },
    })
})
