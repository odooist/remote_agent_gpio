from datetime import datetime
import logging
from odoo import models, fields, api, _

logger = logging.getLogger(__name__)


class Button(models.Model):
    _name = 'remote_agent_gpio.button'
    _description = 'GPIO Button'
    _order = 'gpio_number'

    name = fields.Char(required=True)
    gpio_number = fields.Integer(string='GPIO number', required=True)
    agent = fields.Many2one('remote_agent.agent', required=True)
    agent_uid = fields.Char(related='agent.agent_uid')
    note = fields.Char()
    column = fields.Selection(selection=[('1', '1'), ('2', '2'), ('3', '3')],
                              required=True, default='1')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', _('The name must be unique !')),
        ('check_uniq', 'unique (agent, gpio_number)', _('This bumber is already used!')),
    ]

