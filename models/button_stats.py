from datetime import datetime, timedelta, time
import logging
from odoo import models, fields, api, _

logger = logging.getLogger(__name__)

# RPI reads buttons once per 15 second
PERIOD_MEASUREMENTS = 4 * 60 * 8  # 19200


class ButtonStats(models.Model):
    _name = 'remote_agent_gpio.button_stats'
    _description = 'GPIO Button Stats'
    _order = 'day desc'
    _log_access = False

    button = fields.Many2one('remote_agent_gpio.button', required=True,
                             ondelete='set null')
    last_state = fields.Boolean(readonly=True)
    column = fields.Selection(related='button.column', store=True)
    gpio_number = fields.Integer(related='button.gpio_number')
    agent = fields.Many2one('remote_agent.agent', related='button.agent')
    day = fields.Date()
    # Counters
    period_1_on_states = fields.Integer(string=_('P1 On'))
    period_2_on_states = fields.Integer(string=_('P2 On'))
    period_3_on_states = fields.Integer(string=_('P3 On'))
    period_1_off_states = fields.Integer(string=_('P1 Off'))
    period_2_off_states = fields.Integer(string=_('P2 Off'))
    period_3_off_states = fields.Integer(string=_('P3 Off'))
    daily_on = fields.Integer(compute='_compute_daily', string=_('Total On'))
    daily_off = fields.Integer(compute='_compute_daily', string=_('Total Off'))
    period_1_precision = fields.Float(compute='_compute_period_precision',
                                      store=True, digits=(2, 2),
                                      string=_('P1 Precison'))
    period_2_precision = fields.Float(compute='_compute_period_precision',
                                      store=True,
                                      digits=(2, 2), string=_('P2 Precison'))
    period_3_precision = fields.Float(compute='_compute_period_precision',
                                      store=True,
                                      digits=(2, 2), string=_('P3 Precison'))
    daily_precision = fields.Float(compute='_compute_daily_precision',
                                   digits=(2, 2), store=True,
                                   string=_('Daily Precision'))
    # Percents
    period_1_on_states_pct = fields.Float(compute='_compute_pct', store=True,
                                          digits=(3, 2), string=_('P1 On %'))
    period_2_on_states_pct = fields.Float(compute='_compute_pct', store=True,
                                          digits=(3, 2), string=_('P2 On %'))
    period_3_on_states_pct = fields.Float(compute='_compute_pct', store=True,
                                          digits=(3, 2), string=_('P3 On %'))
    daily_on_pct = fields.Float(compute='_compute_pct', store=True,
                                digits=(3, 2), string=_('D On %'))

    _sql_constraints = [
        ('check_uniq', 'unique (button, day)',
            _('The button already exists for this day!')),
    ]

    @api.model
    def create(self, vals):
        if 'button' not in vals and 'gpio_number' in vals:
            button = self.env['remote_agent_gpio.button'].search([
                ('agent', '=', vals['agent']),
                ('gpio_number', '=', vals['gpio_number'])])
            if button:
                vals['button'] = button.id
        return super(ButtonStats, self).create(vals)

    @api.multi
    @api.depends('period_1_on_states', 'period_2_on_states',
                 'period_3_on_states', 'period_1_off_states',
                 'period_2_off_states', 'period_3_off_states')
    def _compute_daily(self):
        for rec in self:
            rec.daily_on = rec.period_1_on_states + rec.period_2_on_states + \
                rec.period_3_on_states
            rec.daily_off = rec.period_1_off_states + \
                rec.period_2_off_states + rec.period_3_off_states

    @api.multi
    @api.depends('period_1_on_states', 'period_2_on_states',
                 'period_3_on_states', 'period_1_off_states',
                 'period_2_off_states', 'period_3_off_states')
    def _compute_period_precision(self):
        for rec in self:
            rec.period_1_precision = (
                rec.period_1_on_states +
                rec.period_1_off_states) / PERIOD_MEASUREMENTS * 100
            rec.period_2_precision = (
                rec.period_2_on_states +
                rec.period_2_off_states) / PERIOD_MEASUREMENTS * 100
            rec.period_3_precision = (
                rec.period_3_on_states +
                rec.period_3_off_states) / PERIOD_MEASUREMENTS * 100

    def _compute_daily_precision(self):
        for rec in self:
            rec.daily_precision = (
                rec.daily_on + rec.daily_off) / (PERIOD_MEASUREMENTS * 3) * 100

    @api.multi
    @api.depends('period_1_on_states', 'period_2_on_states',
                 'period_3_on_states', 'period_1_off_states',
                 'period_2_off_states', 'period_3_off_states')
    def _compute_pct(self):
        for rec in self:
            # Process on states
            rec.period_1_on_states_pct = (
                rec.period_1_on_states / PERIOD_MEASUREMENTS * 100)
            rec.period_2_on_states_pct = (
                rec.period_2_on_states / PERIOD_MEASUREMENTS * 100)
            rec.period_3_on_states_pct = (
                rec.period_3_on_states / PERIOD_MEASUREMENTS * 100)
            # Compute daily
            rec.daily_on_pct = rec.daily_on / (PERIOD_MEASUREMENTS * 3) * 100

    @api.model
    def vacuum(self, days=365):
        now = datetime.now() - timedelta(days=days)
        recs = self.search(
            [('day', '<', fields.Datetime.to_string(now))])
        recs.unlink()

    @api.model
    def update_stats(self, data):
        days_data = data['data']
        agent = self.env[
            'remote_agent.agent'].sudo()._get_agent_by_uid(data['agent'])
        for day in days_data.keys():
            for button in days_data[day]:
                # Check if we already have stats for this button and this day
                rec = self.env['remote_agent_gpio.button_stats'].search([
                    ('day', '=', day),
                    ('gpio_number', '=', button),
                    ('agent', '=', agent.id)])
                if not rec:
                    try:
                        with self.env.cr.savepoint():
                            rec = self.create({
                                'day': day,
                                'agent': agent.id,
                                'gpio_number': button,
                            })
                    except Exception as e:
                        logger.error('Error creating button: %s', str(e))
                        continue
                # Process rec buttons
                last_state = None
                for period_state in days_data[day][button].keys():
                    period, state = period_state.split('_')[1:]
                    last_state = bool(int(state))
                    state = 'on' if state == '1' else 'off'
                    current = eval(
                        'rec.period_{}_{}_states'.format(period, state))
                    rec.write({
                        'period_{}_{}_states'.format(
                            period, state): current + days_data[
                                day][button][period_state]})
                rec.last_state = last_state
        self.env['bus.bus'].sendone('remote_agent_gpio_button_stats',
                                    {'reload': True})
        return True

    @api.multi
    def refresh(self):
        return True
