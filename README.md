# Installation
## System requirements
* Python pip (apt install python-pip)
* All other python requirements will be installed during the agent installation

## Odoo addons
[Download](https://github.com/litnimax/remote_agent/archive/12.0.zip) and put [Remote Agent](https://github.com/litnimax/remote_agent/tree/12.0) (v12.0) into Odoo addons folder.

[Download](https://gitlab.com/odooist/remote_agent_gpio/-/archive/master/remote_agent_gpio-master.zip) 
and put this addon to the Odoo's addons folder.

Install *Remote Agent* requirements:
```
pip3 install -r remote_agent/requirements.txt
```

Update Odoo's addons and install first `remote_agent`, and then `remote_agent_gpio` addons.

## Agent installation
Download the same [archive](https://gitlab.com/odooist/remote_agent_gpio/-/archive/master/remote_agent_gpio-master.zip) also to RPi box.

Unpack it in `/root` folder.

Run `./install.sh`. This script will download base agent from *Remote Agent* package.

Move `agent` folder to `/srv/agent`.

Adjust `start_agent.sh`, check paths, copy `remote_agent/odoo_agent.service` to `/etc/systemd/system` and activate it.

Here is the complete checklist of installation:

```bash
cd /root
wget https://gitlab.com/odooist/remote_agent_gpio/-/archive/master/remote_agent_gpio-master.zip
unzip remote_agent_gpio-master.zip
rm remote_agent_gpio-master.zip
cd remote_agent_gpio-master/
cd agent
./install.sh
cd ..
mv agent /srv/agent
cd /srv/agent
mcedit start_agent.sh
cp remote_agent/odoo_agent.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable  odoo_agent
systemctl start odoo_agent
systemctl status odoo_agent
```

